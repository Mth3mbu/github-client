import { createAction, props } from '@ngrx/store';
import { SearchResponse } from 'src/app/interfaces/searchReposResponse';
import { SearchRequest } from 'src/app/interfaces/searchRequest';

export const searchStart = createAction(
  '[Repo/API] search start',
  props<SearchRequest>()
);

export const searchFail = createAction(
  '[Repo/API] search fail',
  (response: any) => response
);

export const search = createAction(
  '[Repo/API] search',
  (response: SearchResponse) => response
);

export const setSearchTerm = createAction(
  '[Search] set search term',
  props<{ searchTerm: string }>()
);
