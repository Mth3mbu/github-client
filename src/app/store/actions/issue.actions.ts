import { createAction, props } from '@ngrx/store';
import { Issue } from 'src/app/interfaces/issue';
import { IssuesRequest } from 'src/app/interfaces/issuesRequest';

export const fetchStart = createAction(
  '[Repo/API] fetch issues start',
  props<IssuesRequest>()
);

export const fetchFail = createAction(
  '[Repo/API] fetch issues fail',
  (response: any) => response
);

export const setIssues = createAction(
  '[Repo/API] set issues',
  props<{ issues: Issue[] }>()
);
