import { createReducer, on } from '@ngrx/store';
import * as repoActions from '../actions/repository.actions';
import { AppState } from '../interfaces/appState';
import * as issuesActions from '../actions/issue.actions';

const initialState: AppState = {
  response: {
    items: [],
    incomplete_results: false,
    total_count: 0
  },
  error: '',
  searchTerm: '',
  issues: []
};

export const repositoryReducer = createReducer(
  initialState,
  on(repoActions.search, (state, { incomplete_results, items, total_count }) => ({
    ...state,
    response: {
      items: items,
      incomplete_results: incomplete_results,
      total_count: total_count
    }
  })
  ),
  on(repoActions.searchFail, (state, { response }) => ({
    ...state,
    error: response?.message
  })),

  on(repoActions.setSearchTerm, (state, { searchTerm }) => ({
    ...state,
    error: '',
    searchTerm
  })),
  on(issuesActions.setIssues, (state, { issues }) => ({
    ...state,
    issues
  })
  ), on(issuesActions.fetchFail, (state, { response }) => ({
    ...state,
    error: response?.message
  })),
);

