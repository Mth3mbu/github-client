import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map, catchError, of } from 'rxjs';
import { IssueService } from 'src/app/services/issues.service';
import * as issuesActions from '../actions/issue.actions';

@Injectable()
export class IssuesEffects {
  authLogin = createEffect(() => this.actions$
    .pipe(ofType(issuesActions.fetchStart),
      switchMap(
        (request) => this.issuesService.getRepositoryIssues(request)
          .pipe(
            map(res => issuesActions.setIssues({ issues: res })),
            catchError(error => of(issuesActions.fetchFail(error?.error?.message)))
          )
      )
    ));

  constructor(private actions$: Actions,
    private issuesService: IssueService) { }
}
