import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map, catchError, of } from 'rxjs';
import { RepositoryService } from 'src/app/services/repository.service';
import * as repoActions from '../actions/repository.actions'

@Injectable()
export class RepositoryEffects {
  authLogin = createEffect(() => this.actions$
    .pipe(ofType(repoActions.searchStart),
      switchMap(
        (request) => this.repoServices.searchRepository(request)
          .pipe(
            map(res => {
              this.route.navigate(['repos']);
              return repoActions.search(res);
            }
            ),
            catchError(error => of(repoActions.searchFail(error?.error?.message)))
          )
      )
    ));

  constructor(private actions$: Actions,
    private route: Router,
    private repoServices: RepositoryService) { }
}
