import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '../interfaces/appState';

export const selectState = createFeatureSelector<AppState>('repo');
export const selectIssues = createSelector(selectState, state => state.issues);
