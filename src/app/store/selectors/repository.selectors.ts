import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '../interfaces/appState';

export const selectState = createFeatureSelector<AppState>('repo');
export const selectRepos = createSelector(selectState, state => state.response.items);
export const selectTotalRepos = createSelector(selectState, state => state.response.total_count);
export const selectSearchTerm = createSelector(selectState, state => state.searchTerm);
