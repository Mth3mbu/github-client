import { Issue } from 'src/app/interfaces/issue';

export interface IssuesState {
  issues: Issue[]
}
