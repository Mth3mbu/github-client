import { Issue } from 'src/app/interfaces/issue';
import { SearchResponse } from 'src/app/interfaces/searchReposResponse';

export interface AppState {
  response: SearchResponse,
  error: string,
  searchTerm: string,
  issues: Issue[]
}
