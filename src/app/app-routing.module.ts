import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BacklogListingComponent } from './components/backlog-listing/backlog-listing.component';
import { ReportComponent } from './components/report/report.component';
import { RepositoryDetailsComponent } from './components/repository-details/repository-details.component';
import { RepositoryListingComponent } from './components/repository-listing/repository-listing.component';
import { SearchComponent } from './components/search/search.component';

const routes: Routes = [{ component: SearchComponent, path: '' },
{ component: RepositoryListingComponent, path: 'repos' },
{ component: BacklogListingComponent, path: 'repo/issues' },
{ component: ReportComponent, path: 'repo/issues/report' },
{ component: RepositoryDetailsComponent, path: 'repo/:id' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
