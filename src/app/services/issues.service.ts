import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Issue } from '../interfaces/issue';
import { IssuesRequest } from '../interfaces/issuesRequest';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  constructor(private http: HttpClient) { }

  getRepositoryIssues(request: IssuesRequest) {
    return this.http.get<Issue[]>(`${environment.baseUrl}/repos/${request.userName}/${request.repoName}/issues?state=all`)
  }
}
