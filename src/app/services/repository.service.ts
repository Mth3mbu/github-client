import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SearchResponse } from '../interfaces/searchReposResponse';
import { SearchRequest } from '../interfaces/searchRequest';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  constructor(private http: HttpClient) { }

  searchRepository(request: SearchRequest) {
    return this.http.get<SearchResponse>(`${environment.baseUrl}/search/repositories?q=${request.searchTerm}&page=${request.page}&per_page=${request.rows}`)
  }
}
