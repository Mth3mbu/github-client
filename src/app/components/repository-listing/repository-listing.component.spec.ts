import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepositoryListingComponent } from './repository-listing.component';

describe('RepositoryListingComponent', () => {
  let component: RepositoryListingComponent;
  let fixture: ComponentFixture<RepositoryListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepositoryListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoryListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
