import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { Item } from 'src/app/interfaces/item';
import { AppState } from 'src/app/store/interfaces/appState';
import * as fromRepo from '../../store/selectors/repository.selectors';
import * as repoActions from '../../store/actions/repository.actions';
import { SearchRequest } from 'src/app/interfaces/searchRequest';

@Component({
  selector: 'repository-listing',
  templateUrl: './repository-listing.component.html',
  styleUrls: ['./repository-listing.component.scss']
})
export class RepositoryListingComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  displayedColumns: string[] = ['name', 'description', 'id', 'node_id'];
  dataSource: MatTableDataSource<Item> = new MatTableDataSource();
  totalPages: number = 0;
  rows: number = 20;
  searchTerm: string = '';

  ngOnInit(): void {
    this.store.select(fromRepo.selectState).subscribe(results => {
      this.totalPages = results.response.total_count;
      this.dataSource.data = results.response.items;
      this.searchTerm = results.searchTerm;
    });
  }

  onPageChanged($event: any) {
    const offset = $event.pageIndex * $event.pageSize;
    const request: SearchRequest = { searchTerm: this.searchTerm, page: offset, rows: 15 };
    if (offset < 10001)
      this.store.dispatch(repoActions.searchStart(request));
  }
}
