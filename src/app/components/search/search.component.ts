import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { SearchRequest } from 'src/app/interfaces/searchRequest';
import { AppState } from 'src/app/store/interfaces/appState';
import { SnackBarService } from 'src/app/utility/services/snack-bar.service';
import * as repoActions from '../../store/actions/repository.actions';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(private store: Store<AppState>,
    private snackBar: SnackBarService) { }

  searchTerm: string = '';

  ngOnInit(): void {

  }

  onSearch() {
    if (this.searchTerm !== '' && this.searchTerm !== null && this.searchTerm !== undefined) {
      const request: SearchRequest = { searchTerm: this.searchTerm, page: 0, rows: 15 };
      this.store.dispatch(repoActions.searchStart(request));
      this.store.dispatch(repoActions.setSearchTerm({ searchTerm: this.searchTerm }))
    } else
      this.snackBar.openSnackbar('Please enter a repository name on the above Search box', 'error-snackbar');
  }
}
