import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Item } from 'src/app/interfaces/item';
import { AppState } from 'src/app/store/interfaces/appState';
import * as fromRepo from '../../store/selectors/repository.selectors';

@Component({
  selector: 'app-repository-details',
  templateUrl: './repository-details.component.html',
  styleUrls: ['./repository-details.component.scss']
})
export class RepositoryDetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private store: Store<AppState>) { }
  repository = {} as Item;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const repoId = parseInt(params['id']);
      this.store.select(fromRepo.selectRepos).subscribe(results => {
        this.repository = results.find(repo => repo.id === repoId) ?? {} as Item;
      });
    })
  }
}
