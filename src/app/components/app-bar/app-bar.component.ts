import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/interfaces/appState';
import * as fromRepo from '../../store/selectors/repository.selectors';

@Component({
  selector: 'app-bar',
  templateUrl: './app-bar.component.html',
  styleUrls: ['./app-bar.component.scss']
})
export class AppBarComponent implements OnInit {

  constructor(private store: Store<AppState>) { }
  showReposMenu: boolean = false;
  showIssuesMenu: boolean = false;

  ngOnInit(): void {
    this.store.select(fromRepo.selectState).subscribe(state => {
      this.showReposMenu = state.response.items.length > 0;
      this.showIssuesMenu = state.issues.length > 0;
    });
  }
}
