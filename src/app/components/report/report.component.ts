import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ChartConfiguration, ChartData, ChartType } from 'chart.js';
import DatalabelsPlugin from 'chartjs-plugin-datalabels';
import { IssuesState } from 'src/app/store/interfaces/issuesState';
import * as fromIssues from '../../store/selectors/issues.selectors';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  constructor(private store: Store<IssuesState>) { }

  public pieChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    plugins: {
      legend: {
        display: true,
        position: 'top',
      }
    }
  };

  public pieChartData: ChartData<'pie', number[], string | string[]> = {
    labels: [],
    datasets: [{
      data: []
    }]
  };

  public pieChartType: ChartType = 'pie';
  public pieChartPlugins = [DatalabelsPlugin];

  ngOnInit(): void {
    this.store.select(fromIssues.selectState).subscribe(issues => {
      const totalOpen = issues.issues.filter(issue => issue.state === 'open').length;
      const totalClosed = issues.issues.length - totalOpen;
      this.pieChartData = {
        labels: ['Closed issues', 'Open issues'],
        datasets: [{
          data: [totalOpen, totalClosed]
        }]
      };

    });
  }

  onBackClick() {
    window.history.back();
  }
}
