import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Issue } from 'src/app/interfaces/issue';
import { IssuesRequest } from 'src/app/interfaces/issuesRequest';
import * as issuesActions from '../../store/actions/issue.actions';
import * as fromIssues from '../../store/selectors/issues.selectors';
import { AppState } from 'src/app/store/interfaces/appState';

@Component({
  selector: 'app-backlog-listing',
  templateUrl: './backlog-listing.component.html',
  styleUrls: ['./backlog-listing.component.scss']
})
export class BacklogListingComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private route: ActivatedRoute,
    private store: Store<AppState>) {
    this.paginator = {} as MatPaginator;
  }

  displayedColumns: string[] = ['title', 'state', 'created_at', 'comments'];
  dataSource: MatTableDataSource<Issue> = new MatTableDataSource()
  showReportBtn: boolean = false;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const request: IssuesRequest = { userName: params['user'], repoName: params['repo'] };
      if (request.userName && request.repoName)
        this.store.dispatch(issuesActions.fetchStart(request));
    });

    this.store.select(fromIssues.selectIssues).subscribe(issues => {
      this.dataSource.data = issues;
      this.showReportBtn = issues.length > 0;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
