import { Item } from './item';

export interface SearchResponse {
  total_count: number,
  incomplete_results: boolean,
  items: Item[],
}
