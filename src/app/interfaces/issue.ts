import { Owner } from './owner'

export interface Issue {
  active_lock_reason: string,
  assignee: object,
  assignees: []
  author_association: string,
  body: string,
  closed_at: null
  comments: number,
  comments_url: string,
  created_at: string,
  draft: boolean,
  events_url: string,
  html_url: string,
  id: number,
  labels: []
  labels_url: string,
  locked: boolean,
  milestone: object,
  node_id: string,
  number: null
  performed_via_github_app: string,
  pull_request: object,
  reactions: Object,
  repository_url: string,
  state: string,
  timeline_url: string,
  title: string,
  updated_at: string,
  url: string,
  user: Owner
}
