export interface SearchRequest {
  page: number,
  rows: number,
  searchTerm: string
}
