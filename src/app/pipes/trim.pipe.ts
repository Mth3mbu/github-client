import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trim'
})
export class TrimPipe implements PipeTransform {

  transform(value: string, limit: number, trail = '...'): string {
    if (value === undefined || value === '' || value === null) {
      return 'No description!';
    }

    if (value?.length <= limit) {
      return value;
    }

    return value?.substring(0, limit - trail?.length).replace(/\s+$/, '') + trail;
  }

}
