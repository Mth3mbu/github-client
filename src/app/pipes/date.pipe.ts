import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'date'
})
export class DatePipe implements PipeTransform {

  transform(value: string, showDate: boolean = true): string {
    if (showDate)
      return moment(value).format('D MMM YYYY');

    return moment(value).startOf('day').fromNow();
  }

}
