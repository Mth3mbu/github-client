import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchComponent } from './components/search/search.component';
import { RepositoryListingComponent } from './components/repository-listing/repository-listing.component';
import { RepositoryDetailsComponent } from './components/repository-details/repository-details.component';
import { BacklogListingComponent } from './components/backlog-listing/backlog-listing.component';
import { AppBarComponent } from './components/app-bar/app-bar.component';
import { AngularMaterialModule } from './angular-material.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SpinnerInterceptor } from './inteceptors/spinner.interceptor';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { RepositoryEffects } from './store/effects/Repository.effects';
import { repositoryReducer } from './store/reducers/repository.reducer'
import { IssuesEffects } from './store/effects/issues.effects';
import { NgChartsModule } from 'ng2-charts';
import { ReportComponent } from './components/report/report.component';
import { SnackBarComponent } from './utility/components/snack-bar/snack-bar.component';
import { SpinnerComponent } from './utility/components/spinner/spinner.component';
import { HttpErrorInterceptor } from './inteceptors/http.interceptor';
import { TrimPipe } from './pipes/trim.pipe';
import { DatePipe } from './pipes/date.pipe';


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    RepositoryListingComponent,
    RepositoryDetailsComponent,
    BacklogListingComponent,
    AppBarComponent,
    SpinnerComponent,
    ReportComponent,
    SnackBarComponent,
    TrimPipe,
    DatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    HttpClientModule,
    NgChartsModule,
    StoreModule.forRoot({
      repo: repositoryReducer
    }),
    EffectsModule.forRoot(
      [
        RepositoryEffects,
        IssuesEffects
      ]),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule { }
