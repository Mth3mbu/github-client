# GitHubClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3.

## Development server

1. Clone the repo and open the project from any IDE of your choice, `Visual Studio code recommended`
2. Make sure you have Angular and Node installed
3. Run `npm install` to install all the project `dependencies`
4. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
5. Make sure you run the above commands from your project directory `Command line interface` e.g `C:\Work\code\github-client> npm install`
6. Your good to go.
